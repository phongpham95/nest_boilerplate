import { Field, ObjectType } from 'type-graphql';

@ObjectType()
export class StandardMutationError {
  @Field({ nullable: true })
  public title?: string;

  @Field()
  public message: string;
}
