export const unallowedErr = {
  error: {
    message: 'Không đủ quyền để thực hiện!',
  },
};

export const invalidMutationErr = {
  error: {
    message: 'Yêu cầu không hợp lệ!',
  },
};

export const notExistLocationTypeMutationErr = {
  error: {
    message: 'Loại địa điểm không tồn tại!',
  },
};

export const notExistLocationMutationErr = {
  error: {
    message: 'Địa điểm không tồn tại!',
  },
};

export const existEquipmentInLocation = {
  error: {
    message: 'Thiết bị vẫn còn trong địa điểm',
  },
};

export const failedCreatePropsedEquipment = {
  error: {
    message: 'Tạo phiếu đề xuất không thành công',
  },
};

export const existGoodsInProposedEquipment = {
  error: {
    message: 'Vẫn còn tồn tại phiếu trong đề xuất',
  },
};

export const notExistRoleMutationErr = {
  error: {
    message: 'Chức danh không tồn tại!!',
  },
};

export const notExistEmployeeMutationErr = {
  error: {
    message: 'Nhân viên không tồn tại!',
  },
};

export const notExistProposedEquipmentMutationErr = {
  error: {
    message: 'Đề xuất thiết bị không tồn tại!',
  },
};

export const incorrectRoleMutationErr = {
  error: {
    message: 'Chức danh không đúng!!',
  },
};

export const incorrectPasswordMutationErr = {
  error: {
    message: 'Mật khẩu không đúng!',
  },
};

export const invalidLoginMutationErr = {
  error: {
    message: 'Đăng nhập không hợp lệ!',
  },
};

export const existEmailMutationErr = {
  error: {
    message: 'Email đã tồn tại!',
  },
};

export const existWarehouseMutationErr = {
  error: {
    message: 'Kho đã tồn tại!',
  },
};

export const notExistGoodsReceipt = {
  error: {
    message: 'Phiếu nhập kho không tồn tại!',
  },
};

export const notExistGoodsIssue = {
  error: {
    message: 'Phiếu xuất kho không tồn tại!',
  },
};

export const invalidDataMutationErr = {
  error: {
    message: 'Dữ liệu không hợp lệ!',
  },
};

export const invalidStatusMutationErr = {
  error: {
    message: 'Trạng thái không hợp lệ!',
  },
};

export const duplicateSerialMutationErr = serial => {
  return {
    error: {
      message: `Dữ liệu nhập vào trùng serial! \n Kiểm tra thiết bị serial ${serial} trong kho!`,
    },
  };
};

export const notExistEquipmentModelMutationErr = {
  error: {
    message: 'Thiết bị không tồn tại!',
  },
};

export const notExistRoleMappingMutationErr = {
  error: {
    message: 'RoleMapping không tồn tại!',
  },
};

export const notExistMenuMutationErr = {
  error: {
    message: 'Danh mục không tồn tại!',
  },
};

export const notExistMenuMappingMutationErr = {
  error: {
    message: 'MenuMapping không tồn tại!',
  },
};

export const notExistEquipmentTemp = {
  error: {
    message: 'Thiết bị không tồn tại!',
  },
};

export const notExistEquipment = {
  error: {
    message: 'Thiết bị không tồn tại!',
  },
};

export const existPrefixCode = {
  error: {
    message: 'Mã code đã tồn tại!',
  },
};

export const lengthPrefixCode = {
  error: {
    message: 'Mã nhập kho phải 4 kí tự!',
  },
};

export const equipmentQuantityError = {
  error: {
    message: 'Vượt quá số lượng thiết bị đề xuất!',
  },
};

export const proposedEquipmentAgreementError = {
  error: {
    message: 'Đang có phiếu xuất/nhập chờ duyệt!',
  },
};

export const notExistEquipmentInProposedEquipemntError = {
  error: {
    message: 'Thiết bị không có trong đề xuất!',
  },
};

export const existGoodsReceipt = {
  error: {
    message: 'Còn tồn tại phiếu nhập kho liên quan!',
  },
};

export const existGoodsIssue = {
  error: {
    message: 'Còn tồn tại phiếu xuất kho liên quan!',
  },
};

export const notExistEquipmentModel = {
  error: {
    message: 'Không tồn tại mẫu model thiết bị',
  },
};

export const notExistEquipmentCategory = {
  error: {
    message: 'Không tồn tại mẫu loại thiết bị',
  },
};

export const notExistSupplier = {
  error: {
    message: 'Không tồn tại nhà cung cấp',
  },
};

export const notExistEquipmentHistory = {
  error: {
    message: 'Không tồn tại lịch sử thiết bị',
  },
};

export const existEquipment = {
  error: {
    message: 'Thiết bị trùng serial',
  },
};

export const existProposedEquipment = {
  error: {
    message: 'Thiết bị đã có trong đề xuất ',
  },
};

export const notExistPortalLocation = {
  error: {
    message: 'Không tồn tại địa điểm bên Portal',
  },
};
