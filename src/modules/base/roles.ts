const allRoles = [];

export const PERMISSION_SUPERADMIN = 'superadmin';
allRoles.push({
  name: PERMISSION_SUPERADMIN,
  description: 'SUPERADMIN',
});

export const PERMISSION_AUTHENTICATED = '$authenticated';
allRoles.push({
  name: PERMISSION_AUTHENTICATED,
  description: '$authenticated',
});

export const PERMISSION_EVERYONE = '$everyone';
allRoles.push({
  name: PERMISSION_EVERYONE,
  description: '$everyone',
});

export const PERMISSION_UNAUTHENTICATED = '$unauthenticated';
allRoles.push({
  name: PERMISSION_UNAUTHENTICATED,
  description: '$unauthenticated',
});

export const PERMISSION_READ_ALL = 'all.read';
allRoles.push({
  name: PERMISSION_READ_ALL,
  description: 'can read all',
});

export const PERMISSION_READ_ALL_OF_CUSTOMER = 'allOfCustomer.read';
allRoles.push({
  name: PERMISSION_READ_ALL_OF_CUSTOMER,
  description: 'can read all of customer',
});

export const PERMISSION_CREATE_CAT = 'cat.create';
allRoles.push({
  name: PERMISSION_CREATE_CAT,
  description: 'can create cat',
});

export const PERMISSION_UPDATE_CAT = 'cat.update';
allRoles.push({
  name: PERMISSION_UPDATE_CAT,
  description: 'can update cat',
});

export const PERMISSION_DELETE_CAT = 'cat.delete';
allRoles.push({
  name: PERMISSION_DELETE_CAT,
  description: 'can delete cat',
});

export const PERMISSION_READ_CAT = 'cat.read';
allRoles.push({
  name: PERMISSION_READ_CAT,
  description: 'can read cat',
});

export const PERMISSION_READ_ALL_CATS = 'allCats.read';
allRoles.push({
  name: PERMISSION_READ_ALL_CATS,
  description: 'can read all cats',
});

export const PERMISSION_READ_ALL_CATS_OF_CUSTOMER = 'allCatsOfCustomer.read';
allRoles.push({
  name: PERMISSION_READ_ALL_CATS_OF_CUSTOMER,
  description: 'can read all cats of customer',
});

export const PERMISSION_CREATE_USER = 'user.create';
allRoles.push({
  name: PERMISSION_CREATE_USER,
  description: 'can create user',
});

export const PERMISSION_UPDATE_USER = 'user.update';
allRoles.push({
  name: PERMISSION_UPDATE_USER,
  description: 'can update user',
});

export const PERMISSION_DELETE_USER = 'user.delete';
allRoles.push({
  name: PERMISSION_DELETE_USER,
  description: 'can delete user',
});

export const PERMISSION_READ_USER = 'user.read';
allRoles.push({
  name: PERMISSION_READ_USER,
  description: 'can read user',
});

export const PERMISSION_READ_ALL_USERS = 'allUsers.read';
allRoles.push({
  name: PERMISSION_READ_ALL_USERS,
  description: 'can read all users',
});

export const PERMISSION_READ_ALL_USERS_OF_CUSTOMER = 'allUsersOfCustomer.read';
allRoles.push({
  name: PERMISSION_READ_ALL_USERS_OF_CUSTOMER,
  description: 'can read all users of customer',
});

export const PERMISSION_CREATE_ROLE = 'role.create';
allRoles.push({
  name: PERMISSION_CREATE_ROLE,
  description: 'can create role',
});

export const PERMISSION_UPDATE_ROLE = 'role.update';
allRoles.push({
  name: PERMISSION_UPDATE_ROLE,
  description: 'can update role',
});

export const PERMISSION_DELETE_ROLE = 'role.delete';
allRoles.push({
  name: PERMISSION_DELETE_ROLE,
  description: 'can delete role',
});

export const PERMISSION_READ_ROLE = 'role.read';
allRoles.push({
  name: PERMISSION_READ_ROLE,
  description: 'can read role',
});

export const PERMISSION_CREATE_PROFILE = 'profile.create';
allRoles.push({
  name: PERMISSION_CREATE_PROFILE,
  description: 'can create profile',
});

export const PERMISSION_UPDATE_PROFILE = 'profile.update';
allRoles.push({
  name: PERMISSION_UPDATE_PROFILE,
  description: 'can update profile',
});

export const PERMISSION_DELETE_PROFILE = 'profile.delete';
allRoles.push({
  name: PERMISSION_DELETE_PROFILE,
  description: 'can delete profile',
});

export const PERMISSION_READ_PROFILE = 'profile.read';
allRoles.push({
  name: PERMISSION_READ_PROFILE,
  description: 'can read profile',
});

export const roles = allRoles;
