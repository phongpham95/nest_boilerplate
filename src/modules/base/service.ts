import { Injectable } from '@nestjs/common';
import { get, intersection, set } from 'lodash';
import { OwnersService } from 'modules/entities/owners/owners.service';
import { ObjectId } from 'mongodb';
import { Container } from 'typedi';
import { MongoRepository } from 'typeorm';

import { IAuthInfo } from '../../auth/utils';
import { enforceQueryId } from '../utils/enforceQueryId';
import { PERMISSION_READ_ALL, PERMISSION_READ_ALL_OF_CUSTOMER, PERMISSION_SUPERADMIN } from './roles';

@Injectable()
export class BaseService<TEntity> {
  protected readonly repo: MongoRepository<TEntity>;
  protected readonly ownerServ: OwnersService;

  constructor() {
    Container.set(BaseService, this);
  }

  private readonly options = {
    owner: true,
    customerId: 'customerId',
    creatorId: 'creatorId',
  };

  private async handleModelAccess(where?: any, currentUser?: IAuthInfo) {
    const { roles, user, sub } = currentUser;
    const modelName = get(this.repo, 'metadata.name');

    if (
      intersection(roles, [PERMISSION_SUPERADMIN, PERMISSION_READ_ALL]).length >
      0
    ) {
      return where;
    }

    if (intersection(roles, [`all${modelName}s.read`]).length > 0) {
      return where;
    }

    if (
      user[this.options.customerId] &&
      (intersection(roles, [`all${modelName}sOfCustomer.read`]).length > 0 ||
        intersection(roles, [PERMISSION_READ_ALL_OF_CUSTOMER]).length > 0)
    ) {
      set(where, [this.options.customerId], user[this.options.customerId]);
      return where;
    }

    const owners = await this.ownerServ.findMany({
      select: ['resourceId'],
      where: { modelName, userId: sub },
    });
    const ownerIds = owners.map(o => new ObjectId(o.resourceId));

    where = enforceQueryId(where, '_id', ownerIds);

    return where;
  }

  public async count(where?: any, currentUser?: IAuthInfo) {
    if (currentUser) {
      where = await this.handleModelAccess(where, currentUser);
    }
    return this.repo.count(where);
  }

  public async findById(_id: string, currentUser?: IAuthInfo) {
    return this.findOne({ where: { _id: new ObjectId(_id) } }, currentUser);
  }

  public async findOne(filter?: any, currentUser?: IAuthInfo) {
    if (currentUser) {
      let where = get(filter, 'where', {});
      where = await this.handleModelAccess(where, currentUser);
      set(filter, 'where', where);
    }
    return this.repo.findOne(filter);
  }

  public async findMany(filter?: any, currentUser?: IAuthInfo) {
    if (currentUser) {
      let where = get(filter, 'where', {});
      where = await this.handleModelAccess(where, currentUser);
      set(filter, 'where', where);
    }
    return this.repo.find(filter);
  }

  public async create(record: TEntity) {
    return this.repo.save(record);
  }

  public async updateById(_id: string, record: TEntity) {
    const r = await this.repo.findOne({ where: { _id: new ObjectId(_id) } });
    if (!r) {
      return;
    }
    Object.assign(r, record);
    const data = await this.repo.save(r as any);
    return data;
  }

  public async deleteById(_id: string) {
    const r = await this.repo.findOne({ where: { _id: new ObjectId(_id) } });
    if (!r) {
      return;
    }
    await this.repo.remove(r);
    Object.assign(r, { _id });
    return r;
  }
}
