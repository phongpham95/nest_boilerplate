import { hashSync } from 'bcrypt';
import { set } from 'lodash';
import { getMongoRepository } from 'typeorm';

import { User } from '../entities/users/users.entity';

export const su = {
  name: 'SUPERADMIN',
  email: 'superadmin',
  username: 'superadmin',
  password: '123456',
  profileId: 'superadmin',
};

export const createSu = async () => {
  const userRepo = getMongoRepository(User);
  const user = await userRepo.findOne({ where: { email: 'superadmin' } });
  if (user) {
    set(user, 'password', hashSync(su.password, 10));
    return userRepo.save(user);
  }
  set(su, 'password', hashSync(su.password, 10));
  return userRepo.save(userRepo.create(su));
};
