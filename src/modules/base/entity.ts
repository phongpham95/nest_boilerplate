import { ID, ObjectType } from 'type-graphql';
import { CreateDateColumn, ObjectID, ObjectIdColumn, UpdateDateColumn } from 'typeorm';

import { Field } from '../../modules/graphql/decorators/field';

@ObjectType()
export class BaseEntity {
  @Field(() => ID, { filter: true })
  @ObjectIdColumn()
  public _id: ObjectID;

  @Field({ nullable: true, filter: true, sort: true })
  @CreateDateColumn({ type: 'timestamp' })
  public createdAt?: Date;

  @Field({ nullable: true, filter: true, sort: true })
  @UpdateDateColumn({ type: 'timestamp' })
  public updatedAt?: Date;
}
