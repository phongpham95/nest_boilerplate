import DataLoader = require('dataloader');
import { ObjectId } from 'mongodb';
import { getMongoRepository, MongoRepository } from 'typeorm';

import { BaseEntity } from './entity';

type ClassType<T> = new () => T;

export class DbLoader<T extends BaseEntity> {
  private readonly repo: MongoRepository<T>;
  private readonly loader: DataLoader<string, T>;
  private readonly fieldName: string;

  constructor(Entity: ClassType<T>, fieldName: string = '_id') {
    this.repo = getMongoRepository<T>(Entity);
    this.loader = new DataLoader<string, T>(this._load);
    this.fieldName = fieldName;
  }

  public load = async (key: string): Promise<T> => {
    return this.loader.load(key);
  };

  public loadMany = async (keys: string[]): Promise<T[]> => {
    return this.loader.loadMany(keys);
  };

  private _load = async (keys: string[]): Promise<T[]> => {
    const ids =
      this.fieldName === '_id' ? keys.map(key => new ObjectId(key)) : keys;
    const data = await this.repo.find({
      where: { [this.fieldName]: { $in: ids } },
    });

    const dataMap = data.reduce<{ [key: string]: T }>((all, item) => {
      const fieldValue = item[this.fieldName].toString();
      all[fieldValue] = item;
      return all;
    }, {});

    return keys.map(key => dataMap[key]);
  };
}

export class DbLoaderManager {
  private loaders = new Map<ClassType<BaseEntity>, DbLoader<any>>();

  public getLoader<T extends BaseEntity>(
    Entity: ClassType<T>,
    fieldName?: string,
  ) {
    let loader: DbLoader<T> = this.loaders.get(Entity);
    if (!loader) {
      loader = new DbLoader(Entity, fieldName);
      this.loaders.set(Entity, loader);
    }

    return loader;
  }
}
