import { pickBy } from 'lodash';
import { IContext } from 'modules/graphql/types';
import { ObjectId } from 'mongodb';
import {
  Arg,
  Args,
  ArgsType,
  Authorized,
  ClassType,
  Ctx,
  Field,
  ID,
  InputType,
  Int,
  ObjectType,
  registerEnumType,
  Resolver,
} from 'type-graphql';
import { TypeValueThunk } from 'type-graphql/dist/decorators/types';
import { FieldMetadata } from 'type-graphql/dist/metadata/definitions';
import { getMetadataStorage } from 'type-graphql/dist/metadata/getMetadataStorage';
import Container from 'typedi';

import { StandardMutationError } from '../../modules/error';
import { Mutation, Query } from '../graphql/decorators';
import { BaseEntity } from './entity';
import { PERMISSION_AUTHENTICATED } from './roles';
import { SortDirection } from './type/resolver';
import { OPERATORS_FIELDNAME, processFilters } from './utils';

export type Empty = null | undefined;
export type Status = number;

export const FILTER_METADATA_KEY = 'graphql:filter';
export const SORT_METADATA_KEY = 'graphql:sort';

@ArgsType()
export class FindManyArgs {
  @Field(() => Int, { nullable: true })
  public skip?: number = 0;

  @Field(() => Int, { nullable: true })
  public take?: number;
}

@ArgsType()
export class IdArg {
  @Field(() => ID)
  public _id: ObjectId;
}

interface IOption {
  role?: string[];
  enable?: boolean;
}

interface IPermissions {
  readPermission?: IOption;
  createPermission?: IOption;
  updatePermission?: IOption;
  removePermission?: IOption;
}

interface IOptions {
  permissions?: IPermissions;
}

registerEnumType(SortDirection, { name: 'SortDirection' });

function addArrayOperators(cls: ClassType, getType: TypeValueThunk) {
  Field(() => [getType()], { nullable: true })(cls.prototype, 'in');
  Field(() => [getType()], { nullable: true })(cls.prototype, 'nin');
}
function addRangeOperators(cls: ClassType, getType: TypeValueThunk) {
  Field(() => getType(), { nullable: true })(cls.prototype, 'gt');
  Field(() => getType(), { nullable: true })(cls.prototype, 'gte');
  Field(() => getType(), { nullable: true })(cls.prototype, 'lt');
  Field(() => getType(), { nullable: true })(cls.prototype, 'lte');
}

@InputType()
class StringOperatorArgs {}
addArrayOperators(StringOperatorArgs, () => String);

@InputType()
class NumberOperatorArgs {}
addArrayOperators(NumberOperatorArgs, () => Number);
addRangeOperators(NumberOperatorArgs, () => Number);

@InputType()
class DateOperatorArgs {}
addRangeOperators(DateOperatorArgs, () => Date);

export function createBaseResolver<TEntity extends BaseEntity>(
  EntityCls: ClassType<TEntity>,
  options?: IOptions,
) {
  const entityName = EntityCls.name;
  const {
    permissions: {
      readPermission,
      createPermission,
      updatePermission,
      removePermission,
    },
  } = options;

  @InputType(`${entityName}FilterOperator`)
  class WhereFilterOperatorArgs {}

  @InputType(`${entityName}Filter`)
  class WhereFilterArgs {
    @Field(() => [ID], { nullable: true })
    public _ids: [string];

    @Field(() => WhereFilterArgs, { nullable: true })
    public AND: [WhereFilterArgs];

    @Field(() => WhereFilterArgs, { nullable: true })
    public OR: [WhereFilterArgs];
  }

  @InputType(`${entityName}Sort`)
  class SortFilterArgs {}

  const fields: FieldMetadata[] = (getMetadataStorage() as any).getClassFieldMetadata(
    EntityCls,
  );
  const baseFields: FieldMetadata[] = (getMetadataStorage() as any).getClassFieldMetadata(
    BaseEntity,
  );

  let hasOperator = false;
  [...fields, ...baseFields].forEach(field => {
    const filterEnabled = Reflect.getMetadata(
      FILTER_METADATA_KEY,
      EntityCls.prototype,
      field.name,
    );
    const sortEnabled = Reflect.getMetadata(
      SORT_METADATA_KEY,
      EntityCls.prototype,
      field.name,
    );

    if (filterEnabled) {
      Field(field.getType, { nullable: true })(
        WhereFilterArgs.prototype,
        field.name,
      );
      if (field.getType() === String) {
        hasOperator = true;
        Field(() => StringOperatorArgs, { nullable: true })(
          WhereFilterOperatorArgs.prototype,
          field.name,
        );
      }
      if (field.getType() === Date) {
        hasOperator = true;
        Field(() => DateOperatorArgs, { nullable: true })(
          WhereFilterOperatorArgs.prototype,
          field.name,
        );
      }
      if (field.getType() === Number) {
        hasOperator = true;
        Field(() => NumberOperatorArgs, { nullable: true })(
          WhereFilterOperatorArgs.prototype,
          field.name,
        );
      }
    }
    if (sortEnabled) {
      Field(() => SortDirection, { nullable: true })(
        SortFilterArgs.prototype,
        field.name,
      );
    }
  });

  if (hasOperator) {
    Field(() => WhereFilterOperatorArgs, { nullable: true })(
      WhereFilterArgs.prototype,
      OPERATORS_FIELDNAME,
    );
  }

  @ArgsType()
  class FindOneFilterArgs {
    @Field(() => WhereFilterArgs, { nullable: true })
    public where: WhereFilterArgs;

    @Field(() => SortFilterArgs, { nullable: true })
    public order: SortFilterArgs;
  }

  @ArgsType()
  class FindManyFilterArgs {
    @Field(() => Int, { nullable: true })
    public skip?: number = 0;

    @Field(() => Int, { nullable: true })
    public take?: number;

    @Field(() => WhereFilterArgs, { nullable: true })
    public where?: WhereFilterArgs;

    @Field(() => SortFilterArgs, { nullable: true })
    public order?: SortFilterArgs;
  }

  @ArgsType()
  class CountArgs {
    @Field(() => WhereFilterArgs, { nullable: true })
    public where?: WhereFilterArgs;
  }

  @ArgsType()
  class UpdateArgs extends IdArg {
    @Field(() => EntityCls)
    public record: TEntity;
  }

  @ObjectType(`${entityName}BasePayload`)
  class BasePayload {
    @Field(() => EntityCls, { nullable: true })
    public data: TEntity;

    @Field({ nullable: true })
    public error: StandardMutationError;
  }

  @Resolver({ isAbstract: true })
  class BaseResolver {
    protected readonly service: any;

    constructor() {
      Container.set(BaseResolver, this);
    }

    @Authorized(readPermission.role || PERMISSION_AUTHENTICATED)
    @Query(() => EntityCls, {
      name: `find${entityName}ById`,
      nullable: true,
      enable: readPermission.enable,
    })
    public async findById(
      @Arg('_id') _id: string,
      @Ctx() { authInfo }: IContext,
    ) {
      let currentUser = null;
      if (fields.some(f => f.name === 'creatorId')) {
        currentUser = authInfo;
      }

      return this.service.findById(_id, currentUser);
    }

    @Authorized(readPermission.role || PERMISSION_AUTHENTICATED)
    @Query(() => EntityCls, {
      nullable: true,
      name: `findOne${entityName}`,
      enable: readPermission.enable,
    })
    public async findOne(
      @Args() args: FindOneFilterArgs,
      @Ctx() { authInfo }: IContext,
    ) {
      const { where: filter, order } = args;

      const where = processFilters(pickBy(filter, val => !!val));

      let currentUser = null;
      if (fields.some(f => f.name === 'creatorId')) {
        currentUser = authInfo;
      }

      return this.service.findOne(
        {
          where,
          ...(order && { order }),
        },
        currentUser,
      );
    }

    @Authorized(readPermission.role || PERMISSION_AUTHENTICATED)
    @Query(() => [EntityCls], {
      name: `findMany${entityName}`,
      enable: readPermission.enable,
    })
    public async findMany(
      @Args() args: FindManyFilterArgs,
      @Ctx() { authInfo }: IContext,
    ) {
      const { skip, take, order, where: filter } = args;

      const paging = {
        ...(skip && { skip }),
        ...(take && { take }),
      };
      const where = processFilters(pickBy(filter, val => !!val));

      let currentUser = null;
      if (fields.some(f => f.name === 'creatorId')) {
        currentUser = authInfo;
      }

      return this.service.findMany(
        {
          ...paging,
          where,
          ...(order && { order }),
        },
        currentUser,
      );
    }

    @Authorized(readPermission.role || PERMISSION_AUTHENTICATED)
    @Query(() => Int, {
      nullable: true,
      name: `count${entityName}`,
      enable: readPermission.enable,
    })
    public async count(
      @Args() { where: filter }: CountArgs,
      @Ctx() { authInfo }: IContext,
    ): Promise<number> {
      const where = processFilters(pickBy(filter, val => !!val));

      let currentUser = null;
      if (fields.some(f => f.name === 'creatorId')) {
        currentUser = authInfo;
      }

      return this.service.count(where, currentUser);
    }

    @Authorized(createPermission.role || PERMISSION_AUTHENTICATED)
    @Mutation(() => BasePayload, {
      name: `create${entityName}`,
      enable: createPermission.enable,
    })
    public async create(
      @Arg('record', () => EntityCls) record: TEntity,
    ) {
      const data = await this.service.create(record);
      return { data };
    }

    @Authorized(updatePermission.role || PERMISSION_AUTHENTICATED)
    @Mutation(() => BasePayload, {
      name: `update${entityName}ById`,
      enable: updatePermission.enable,
    })
    public async updateById(@Args() { _id, record }: UpdateArgs) {
      const data = await this.service.updateById(_id, record);
      return { data };
    }

    @Authorized(removePermission.role || PERMISSION_AUTHENTICATED)
    @Mutation(() => BasePayload, {
      name: `remove${entityName}ById`,
      enable: removePermission.enable,
    })
    public async deleteById(@Args() { _id }: IdArg) {
      const data = await this.service.deleteById(_id);
      return { data };
    }
  }

  // workaround for generics conflict
  return {
    BaseResolver: BaseResolver as any,
    WhereFilterArgs: WhereFilterArgs as any,
    SortFilterArgs: SortFilterArgs as any,
    FindOneFilterArgs: FindOneFilterArgs as any,
    FindManyFilterArgs: FindManyFilterArgs as any,
  };
}
