import { Field, ObjectType } from 'type-graphql';

import { StandardMutationError } from '../../../modules/error';
import { User } from '../users/users.entity';

@ObjectType()
export class UserPayload {
  @Field({ nullable: true })
  public data?: User;

  @Field({ nullable: true })
  public error?: StandardMutationError;
}
