import { Field, ObjectType } from 'type-graphql';

import { UserPayload } from './user.payload';

@ObjectType()
export class LoginPayload extends UserPayload {
  @Field({ nullable: true })
  public authToken?: string;
}
