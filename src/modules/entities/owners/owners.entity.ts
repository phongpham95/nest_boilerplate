import { ArgsType, ID, InputType, ObjectType } from 'type-graphql';
import { Column, Entity, ObjectID, ObjectIdColumn } from 'typeorm';

import { Field } from '../../../modules/graphql/decorators';

@ArgsType()
@InputType('OwnerInput')
@Entity('Owner')
@ObjectType()
export class Owner {
  @Field(() => ID, { filter: true })
  @ObjectIdColumn()
  public _id: ObjectID;

  @Column()
  @Field()
  public userId: string;

  @Column()
  @Field()
  public resourceId: string;

  @Column()
  @Field()
  public modelName: string;
}
