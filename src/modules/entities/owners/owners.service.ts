import { Container } from 'typedi';
import { MongoRepository } from 'typeorm';

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { BaseService } from '../../../modules/base/service';
import { Owner } from './owners.entity';

@Injectable()
export class OwnersService extends BaseService<Owner> {
  constructor(
    @InjectRepository(Owner)
    protected readonly repo: MongoRepository<Owner>,
  ) {
    super();
    Container.set(OwnersService, this);
  }
}
