import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Owner } from './owners.entity';
import { OwnersService } from './owners.service';

@Module({
  imports: [TypeOrmModule.forFeature([Owner])],
  providers: [OwnersService],
  exports: [OwnersService],
})
export class OwnersModule {}
