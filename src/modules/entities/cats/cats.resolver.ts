import { IContext } from 'modules/graphql/types';
import { Ctx, FieldResolver, Resolver, Root } from 'type-graphql';
import { Container } from 'typedi';

import { User } from '../users/users.entity';
import { BaseCatResolver } from './cats.base';
import { Cat } from './cats.entity';
import { CatsService } from './cats.service';

@Resolver(Cat)
export class CatsResolver extends BaseCatResolver<Cat> {
  constructor(protected readonly service: CatsService) {
    super();
    Container.set(CatsResolver, this);
  }

  @FieldResolver(() => User)
  async creator(@Root() cat: Cat, @Ctx() { loaderManager }: IContext) {
    if (!cat.creatorId) {
      return;
    }
    const loader = loaderManager.getLoader(User);
    return loader.load(cat.creatorId);
  }

  @FieldResolver(() => User)
  async customer(@Root() cat: Cat, @Ctx() { loaderManager }: IContext) {
    if (!cat.customerId) {
      return;
    }
    const loader = loaderManager.getLoader(User);
    return loader.load(cat.customerId);
  }
}
