import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { OwnersModule } from '../owners/owners.module';
import { UsersModule } from '../users/users.module';
import { Cat } from './cats.entity';
import { CatsResolver } from './cats.resolver';
import { CatsService } from './cats.service';
import { CatsSubscriber } from './cats.subscriber';

@Module({
  imports: [TypeOrmModule.forFeature([Cat]), UsersModule, OwnersModule],
  providers: [CatsResolver, CatsService, CatsSubscriber],
  exports: [CatsService],
})
export class CatsModule {}
