import { ArgsType, InputType, Int, ObjectType } from 'type-graphql';
import { Column, Entity } from 'typeorm';

import { CreatorColumn } from '../../../database/decorators/CreatorColumn';
import { CustomerColumn } from '../../../database/decorators/CustomerColumn';
import { Field } from '../../../modules/graphql/decorators';
import { BaseEntity } from '../../base/entity';

@ArgsType()
@InputType('CatInput')
@Entity('Cat')
@ObjectType()
export class Cat extends BaseEntity {
  @Column()
  @Field({ filter: true })
  public name: string;

  @Column()
  @Field(() => Int, { nullable: true, filter: true, sort: true })
  public age?: number;

  @Column()
  @Field({ nullable: true, filter: true })
  public breed?: string;

  @CreatorColumn()
  @Field({ nullable: true })
  public creatorId: string;

  @CustomerColumn()
  @Field({ nullable: true })
  public customerId: string;
}
