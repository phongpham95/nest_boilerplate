import { Container } from 'typedi';
import { MongoRepository } from 'typeorm';

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { BaseService } from '../../../modules/base/service';
import { OwnersService } from '../owners/owners.service';
import { Cat } from './cats.entity';

@Injectable()
export class CatsService extends BaseService<Cat> {
  constructor(
    @InjectRepository(Cat)
    protected readonly repo: MongoRepository<Cat>,
    protected readonly ownerServ: OwnersService,
  ) {
    super();
    Container.set(CatsService, this);
  }
}
