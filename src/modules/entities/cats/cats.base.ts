import {
  PERMISSION_CREATE_CAT,
  PERMISSION_DELETE_CAT,
  PERMISSION_READ_ALL,
  PERMISSION_READ_ALL_CATS,
  PERMISSION_READ_CAT,
  PERMISSION_UPDATE_CAT,
} from '../../../modules/base/roles';
import { createBaseResolver } from '../../base/resolver';
import { Cat } from './cats.entity';

const { BaseResolver: BaseCatResolver } = createBaseResolver<Cat>(Cat, {
  permissions: {
    readPermission: { role: [PERMISSION_READ_CAT, PERMISSION_READ_ALL_CATS, PERMISSION_READ_ALL] },
    createPermission: { role: [PERMISSION_CREATE_CAT] },
    updatePermission: { role: [PERMISSION_UPDATE_CAT] },
    removePermission: { role: [PERMISSION_DELETE_CAT] },
  },
});

export { BaseCatResolver };
