import { get, set } from 'lodash';
import { ObjectId } from 'mongodb';
import { EntitySubscriberInterface, InsertEvent, MongoRepository, RemoveEvent } from 'typeorm';

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Owner } from '../owners/owners.entity';
import { OwnersService } from '../owners/owners.service';
import { User } from '../users/users.entity';
import { UsersService } from '../users/users.service';
import { Cat } from './cats.entity';

@Injectable()
export class CatsSubscriber implements EntitySubscriberInterface<Cat> {
  constructor(
    @InjectRepository(Cat)
    protected readonly repo: MongoRepository<Cat>,
    protected readonly userService: UsersService,
    protected readonly ownerService: OwnersService,
  ) {
    this.repo.manager.connection.subscribers.push(this);
  }

  public listenTo() {
    return Cat;
  }

  // public async beforeInsert(event: InsertEvent<Cat>) {
  //   const { entity } = event;

  //   const creator = (await this.repo.findOne({
  //     where: { _id: new ObjectId(get(entity, 'creatorId')) },
  //   })) as User;

  //   if (creator.customerId) {
  //     set(entity, 'customerId', creator.customerId);
  //   }
  // }

  public async afterInsert(event: InsertEvent<Cat>) {
    const { entity, metadata } = event;

    const owner = {
      userId: entity.creatorId,
      modelName: metadata.name,
      resourceId: entity._id.toString(),
    } as Owner;
    await this.ownerService.create(owner);
  }

  public async afterRemove(event: RemoveEvent<Cat>) {
    const { entityId, databaseEntity, metadata } = event;
    const data = await this.ownerService.findOne({
      where: {
        userId: databaseEntity.creatorId,
        modelName: metadata.name,
        resourceId: entityId.toString(),
      },
    });
    if (!data) {
      return;
    }
    await this.ownerService.deleteById(data._id.toString());
  }
}
