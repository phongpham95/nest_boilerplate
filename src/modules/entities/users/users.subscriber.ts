import { get, set } from 'lodash';
import { ObjectId } from 'mongodb';
import { EntitySubscriberInterface, InsertEvent, MongoRepository } from 'typeorm';

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { User } from './users.entity';
import { UsersService } from './users.service';

@Injectable()
export class UsersSubscriber implements EntitySubscriberInterface<User> {
  constructor(
    @InjectRepository(User)
    protected readonly repo: MongoRepository<User>,
    protected readonly userService: UsersService,
  ) {
    this.repo.manager.connection.subscribers.push(this);
  }

  public listenTo() {
    return User;
  }

  // public async beforeInsert(event: InsertEvent<User>) {
  //   const { entity } = event;

  //   const creatorId = get(entity, 'creatorId');
  //   if (creatorId) {
  //     const creator = (await this.repo.findOne({
  //       where: { _id: new ObjectId(creatorId) },
  //     })) as User;

  //     const customerId = get(creator, 'customerId');
  //     if (customerId) {
  //       set(entity, 'customerId', creator.customerId);
  //     }
  //   }
  // }

  public async afterInsert(event: InsertEvent<User>) {
    const { entity } = event;

    if (entity.profileId !== 'superadmin' && !entity.customerId) {
      set(entity, 'customerId', entity._id.toString());
      await this.repo.save(entity);
    }
  }
}
