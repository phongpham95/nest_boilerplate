import { set } from 'lodash';
import { IContext } from 'modules/graphql/types';
import { Arg, Authorized, Ctx, FieldResolver, Mutation, Query, Resolver, Root } from 'type-graphql';
import Container from 'typedi';

import { PERMISSION_AUTHENTICATED } from '../../../modules/base/roles';
import { LoginPayload, UserPayload } from '../payloads';
import { Profile } from '../profiles/profiles.entity';
import { BaseUserResolver } from './users.base';
import { User } from './users.entity';
import { UsersService } from './users.service';

@Resolver(User)
export class UsersResolver extends BaseUserResolver<User> {
  constructor(private readonly service: UsersService) {
    super();
    Container.set(UsersResolver, this);
  }

  @FieldResolver(() => Profile)
  async profile(@Root() user: User, @Ctx() { loaderManager }: IContext) {
    if (!user.profileId) {
      return;
    }
    const loader = loaderManager.getLoader(Profile, 'name');
    return loader.load(user.profileId);
  }

  @FieldResolver(() => User)
  async creator(@Root() user: User, @Ctx() { loaderManager }: IContext) {
    if (!user.creatorId) {
      return;
    }
    const loader = loaderManager.getLoader(User);
    return loader.load(user.creatorId);
  }

  @FieldResolver(() => User)
  async customer(@Root() user: User, @Ctx() { loaderManager }: IContext) {
    if (!user.customerId) {
      return;
    }
    const loader = loaderManager.getLoader(User);
    return loader.load(user.customerId);
  }

  @FieldResolver()
  protected password(): string {
    return '';
  }

  @Mutation(() => UserPayload)
  async createUser(@Arg('record') record: User, @Ctx() ctx: IContext) {
    const {
      authInfo: { sub },
    } = ctx;

    set(record, 'creatorId', sub);

    return this.service.createUser(record);
  }

  @Mutation(() => LoginPayload)
  async login(
    @Arg('username') username: string,
    @Arg('password') password: string,
  ) {
    return this.service.login(username, password);
  }

  @Authorized(PERMISSION_AUTHENTICATED)
  @Query(() => UserPayload)
  async me(@Ctx() ctx: IContext) {
    return this.service.findMe(ctx);
  }
}
