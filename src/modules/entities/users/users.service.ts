import { compare, hashSync } from 'bcrypt';
import { sign } from 'jsonwebtoken';
import { ObjectId } from 'mongodb';
import { Container } from 'typedi';
import { MongoRepository } from 'typeorm';

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { jwtDefaultExpire, jwtSecret } from '../../../auth/utils';
import { BaseService } from '../../../modules/base/service';
import { invalidLoginMutationErr, invalidMutationErr } from '../../../modules/error';
import { IContext } from '../../../modules/graphql/types';
import { OwnersService } from '../owners/owners.service';
import { LoginPayload, UserPayload } from '../payloads';
import { ProfilesService } from '../profiles/profiles.service';
import { Role } from '../roles/roles.entity';
import { User } from './users.entity';

@Injectable()
export class UsersService extends BaseService<User> {
  constructor(
    @InjectRepository(User)
    protected readonly repo: MongoRepository<User>,
    protected readonly ownerServ: OwnersService,
    private readonly profileServ: ProfilesService,
  ) {
    super();
    Container.set(UsersService, this);
  }

  public signToken = (user: User, roles: Role[]) =>
    sign(
      {
        user,
        roles,
      },
      Buffer.from(jwtSecret, 'base64'),
      {
        subject: user._id.toString(),
        expiresIn: jwtDefaultExpire,
      },
    );

  async login(username: string, password: string): Promise<LoginPayload> {
    let roles = [];

    const user = await this.repo.findOne({
      where: { username },
    });
    if (!user) {
      return invalidLoginMutationErr;
    }

    const isMatched = await compare(password, user.password);

    if (!isMatched) {
      return invalidLoginMutationErr;
    }

    const profile = await this.profileServ.findOne({
      where: { name: user.profileId },
    });
    if (!profile) {
      return invalidLoginMutationErr;
    }

    roles = profile.roles;

    const authToken = this.signToken(user, roles);

    return {
      data: user,
      authToken,
    };
  }

  async createUser(record: User): Promise<UserPayload> {
    const { username, password } = record;

    const employee = await this.repo.findOne({
      where: { username },
    });
    if (employee) {
      return invalidMutationErr;
    }

    const createdEmployee = this.repo.create({
      ...record,
      password: hashSync(password, 10),
    });
    const result = await this.repo.save(createdEmployee);

    return {
      data: result,
    };
  }

  async findMe(ctx: IContext): Promise<UserPayload> {
    const userId = ctx.authInfo.sub;

    const user = await this.repo.findOne({
      where: { _id: new ObjectId(userId) },
    });
    if (!user) {
      return invalidMutationErr;
    }

    return {
      data: user,
    };
  }
}
