import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { OwnersModule } from '../owners/owners.module';
import { Profile } from '../profiles/profiles.entity';
import { ProfilesModule } from '../profiles/profiles.module';
import { User } from './users.entity';
import { UsersResolver } from './users.resolver';
import { UsersService } from './users.service';
import { UsersSubscriber } from './users.subscriber';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Profile]),
    ProfilesModule,
    OwnersModule,
  ],
  providers: [UsersResolver, UsersService, UsersSubscriber],
  exports: [UsersService],
})
export class UsersModule {}
