import {
    PERMISSION_CREATE_USER, PERMISSION_DELETE_USER, PERMISSION_READ_ALL, PERMISSION_READ_ALL_USERS,
    PERMISSION_READ_USER, PERMISSION_UPDATE_USER
} from '../../../modules/base/roles';
import { createBaseResolver } from '../../base/resolver';
import { User } from './users.entity';

const { BaseResolver: BaseUserResolver } = createBaseResolver<User>(User, {
  permissions: {
    readPermission: { role: [PERMISSION_READ_USER, PERMISSION_READ_ALL_USERS, PERMISSION_READ_ALL] },
    createPermission: { role: [PERMISSION_CREATE_USER], enable: false },
    updatePermission: { role: [PERMISSION_UPDATE_USER] },
    removePermission: { role: [PERMISSION_DELETE_USER] },
  },
});

export { BaseUserResolver };
