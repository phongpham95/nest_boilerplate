import { hashSync } from 'bcrypt';
import { InputType, ObjectType } from 'type-graphql';
import { BeforeUpdate, Column, Entity } from 'typeorm';

import { CreatorColumn } from '../../../database/decorators/CreatorColumn';
import { CustomerColumn } from '../../../database/decorators/CustomerColumn';
import { Field } from '../../../modules/graphql/decorators';
import { BaseEntity } from '../../base/entity';

@InputType('UserInput')
@Entity('User')
@ObjectType()
export class User extends BaseEntity {
  @Column()
  @Field({ filter: true })
  public name: string;

  @Column()
  @Field({ filter: true })
  public email: string;

  @Column({ unique: true })
  @Field({ filter: true })
  public username: string;

  @Column()
  @Field()
  public password: string;

  @Column()
  @Field()
  public profileId: string;

  @CreatorColumn()
  @Field({ nullable: true })
  public creatorId: string;

  @CustomerColumn()
  @Field({ nullable: true })
  public customerId: string;

  @BeforeUpdate()
  public hashPassword() {
    if (!this.password) {
      return;
    }
    if (this.password.indexOf('$2b$') < 0) {
      this.password = hashSync(this.password, 10);
    }
  }
}
