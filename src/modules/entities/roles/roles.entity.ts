import { ArgsType, Field, InputType, ObjectType } from 'type-graphql';
import { Column, Entity } from 'typeorm';

import { BaseEntity } from '../../base/entity';

@ArgsType()
@InputType('RoleInput')
@Entity('Role')
@ObjectType()
export class Role extends BaseEntity {
  @Field()
  @Column({ unique: true })
  public name: string;

  @Field()
  @Column()
  public description: string;
}
