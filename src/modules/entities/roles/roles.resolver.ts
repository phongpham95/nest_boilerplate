import { Resolver } from 'type-graphql';
import { Container } from 'typedi';

import { BaseRoleResolver } from './roles.base';
import { Role } from './roles.entity';
import { RolesService } from './roles.service';

@Resolver(Role)
export class RolesResolver extends BaseRoleResolver<Role> {
  constructor(protected readonly service: RolesService) {
    super();
    Container.set(RolesResolver, this);
  }
}
