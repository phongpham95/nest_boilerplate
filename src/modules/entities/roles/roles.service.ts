import { Container } from 'typedi';
import { MongoRepository } from 'typeorm';

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { roles } from '../../../modules/base/roles';
import { BaseService } from '../../../modules/base/service';
import { Role } from './roles.entity';

@Injectable()
export class RolesService extends BaseService<Role> {
  constructor(
    @InjectRepository(Role)
    protected readonly repo: MongoRepository<Role>,
  ) {
    super();
    Container.set(RolesService, this);
  }

  public async addAllRoles() {
    await this.repo.deleteMany({});
    return this.repo.save(this.repo.create(roles));
  }
}
