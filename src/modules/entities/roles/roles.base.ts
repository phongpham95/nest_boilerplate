import {
    PERMISSION_CREATE_ROLE, PERMISSION_DELETE_ROLE, PERMISSION_READ_ALL, PERMISSION_READ_ROLE,
    PERMISSION_UPDATE_ROLE
} from '../../../modules/base/roles';
import { createBaseResolver } from '../../base/resolver';
import { Role } from './roles.entity';

const { BaseResolver: BaseRoleResolver } = createBaseResolver<Role>(Role, {
  permissions: {
    readPermission: { role: [PERMISSION_READ_ROLE, PERMISSION_READ_ALL] },
    createPermission: { role: [PERMISSION_CREATE_ROLE] },
    updatePermission: { role: [PERMISSION_UPDATE_ROLE] },
    removePermission: { role: [PERMISSION_DELETE_ROLE] },
  },
});

export { BaseRoleResolver };
