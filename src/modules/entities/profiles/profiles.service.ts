import { Container } from 'typedi';
import { MongoRepository } from 'typeorm';

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { profiles } from '../../../modules/base/profiles';
import { BaseService } from '../../../modules/base/service';
import { Profile } from './profiles.entity';

@Injectable()
export class ProfilesService extends BaseService<Profile> {
  constructor(
    @InjectRepository(Profile)
    protected readonly repo: MongoRepository<Profile>,
  ) {
    super();
    Container.set(ProfilesService, this);
  }

  public async addAllProfiles() {
    await this.repo.deleteMany({});
    return this.repo.save(this.repo.create(profiles));
  }
}
