import {
    PERMISSION_CREATE_PROFILE, PERMISSION_DELETE_PROFILE, PERMISSION_READ_ALL,
    PERMISSION_READ_PROFILE, PERMISSION_UPDATE_PROFILE
} from '../../../modules/base/roles';
import { createBaseResolver } from '../../base/resolver';
import { Profile } from './profiles.entity';

const { BaseResolver: BaseProfileResolver } = createBaseResolver<Profile>(
  Profile,
  {
    permissions: {
      readPermission: { role: [PERMISSION_READ_PROFILE, PERMISSION_READ_ALL] },
      createPermission: { role: [PERMISSION_CREATE_PROFILE] },
      updatePermission: { role: [PERMISSION_UPDATE_PROFILE] },
      removePermission: { role: [PERMISSION_DELETE_PROFILE] },
    },
  },
);

export { BaseProfileResolver };
