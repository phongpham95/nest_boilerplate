import { Resolver } from 'type-graphql';
import { Container } from 'typedi';

import { BaseProfileResolver } from './profiles.base';
import { Profile } from './profiles.entity';
import { ProfilesService } from './profiles.service';

@Resolver(Profile)
export class ProfilesResolver extends BaseProfileResolver<Profile> {
  constructor(protected readonly service: ProfilesService) {
    super();
    Container.set(ProfilesResolver, this);
  }
}
