import { ArgsType, Field, InputType, ObjectType } from 'type-graphql';
import { Column, Entity } from 'typeorm';

import { BaseEntity } from '../../base/entity';

@ArgsType()
@InputType('ProfileInput')
@Entity('Profile')
@ObjectType()
export class Profile extends BaseEntity {
  @Field()
  @Column({ unique: true })
  public name: string;

  @Field()
  @Column({ unique: true })
  public display: string;

  @Field({ nullable: true })
  @Column()
  public description?: string;

  @Field(() => String)
  @Column()
  public roles: string[];
}
