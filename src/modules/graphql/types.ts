import { IAuthInfo } from '../../auth/utils';
import { DbLoaderManager } from '../../modules/base/dataloader';

export interface IContext {
  authInfo: IAuthInfo;
  loaderManager: DbLoaderManager;
}
