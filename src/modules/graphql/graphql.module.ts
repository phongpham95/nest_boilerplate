import { IncomingMessage } from 'http';
import { buildSchema } from 'type-graphql';
import { Container } from 'typedi';

import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';

import { authChecker } from '../../auth/auth-checker';
import { getCurrentUserInfo } from '../../auth/utils';
import { DbLoaderManager } from '../../modules/base/dataloader';

interface IContextInput {
  req: IncomingMessage;
}
@Module({
  imports: [
    GraphQLModule.forRootAsync({
      useFactory: async () => {
        const schema = await buildSchema({
          authChecker,
          resolvers: [() => null],
          container: Container,
        });
        const config = {
          schema,
          context: async ({ req }: IContextInput) => ({
            loaderManager: new DbLoaderManager(),
            authInfo: await getCurrentUserInfo(req),
          }),
        };
        return config;
      },
    }),
  ],
})
export class GraphModule {}
