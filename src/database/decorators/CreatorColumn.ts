import { BeforeInsert, Column } from 'typeorm';

import { RequestContext } from '../../modules/context/request-context';

const CreatorColumn = () => {
    return (target: any, field: string) => {
        target.__setCreator__ = async function __setCreator__() {
            const authInfo = await RequestContext.authInfo();
            this[field] = authInfo && authInfo.sub;
        }

        BeforeInsert()(target, '__setCreator__');
        Column()(target, field);
    }
}

export { CreatorColumn };