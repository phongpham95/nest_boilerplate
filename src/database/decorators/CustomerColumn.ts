import { get } from 'lodash';
import { BeforeInsert, Column } from 'typeorm';

import { RequestContext } from '../../modules/context/request-context';

const CustomerColumn = () => {
    return (target: any, field: string) => {
        target.__setCustomer__ = async function __setCustomer__() {
            const authInfo = await RequestContext.authInfo();
            this[field] = get(authInfo, 'user.customerId');
        }

        BeforeInsert()(target, '__setCustomer__');
        Column()(target, field);
    }
}

export { CustomerColumn };