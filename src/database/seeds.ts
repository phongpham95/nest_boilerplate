import 'reflect-metadata';

import { NestFactory } from '@nestjs/core';
import { shim } from 'promise.prototype.finally';

import { AppModule } from '../app.module';
import { createSu } from '../modules/base/su';
import { ProfilesModule } from '../modules/entities/profiles/profiles.module';
import { ProfilesService } from '../modules/entities/profiles/profiles.service';
import { RolesModule } from '../modules/entities/roles/roles.module';
import { RolesService } from '../modules/entities/roles/roles.service';

shim();

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // add all roles
  const roleServ = app.select(RolesModule).get(RolesService);
  await roleServ.addAllRoles();
  // add all profiles
  const profileServ = app.select(ProfilesModule).get(ProfilesService);
  await profileServ.addAllProfiles();
  // add su
  await createSu();
}

bootstrap()
  .catch(console.log)
  .finally(() => process.exit(0));
