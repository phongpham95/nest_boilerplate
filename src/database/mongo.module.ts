import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Cat } from '../modules/entities/cats/cats.entity';
import { Owner } from '../modules/entities/owners/owners.entity';
import { Profile } from '../modules/entities/profiles/profiles.entity';
import { Role } from '../modules/entities/roles/roles.entity';
import { User } from '../modules/entities/users/users.entity';

const entities = [Cat, User, Role, Profile, Owner];

@Module({
  imports: [
    TypeOrmModule.forRoot({
      entities,
      type: 'mongodb',
      database: 'nest',
      host: 'localhost',
      port: 27017,
      synchronize: true,
      useNewUrlParser: true,
      keepConnectionAlive: true,
      loggerLevel: 'warn',
    }),
  ],
})
export class MongoModule {}
