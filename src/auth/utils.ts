import { IncomingMessage } from 'http';
import { verify } from 'jsonwebtoken';
import { getMongoRepository } from 'typeorm';

import {
    PERMISSION_AUTHENTICATED, PERMISSION_EVERYONE, PERMISSION_UNAUTHENTICATED
} from '../modules/base/roles';
import { Profile } from '../modules/entities/profiles/profiles.entity';
import { User } from '../modules/entities/users/users.entity';

export interface IAuthInfo {
  user?: User;
  iat?: number;
  exp?: number;
  sub?: string;
  roles: string[];
}

export const jwtSecret = process.env.JWT_SECRET || 'M3gaNet';
export const jwtDefaultExpire = 3600 * 48;

export const getCurrentUserInfo = async (req: IncomingMessage) => {
  const { headers } = req;

  let authInfo = (req as any).authInfo;
  if (authInfo) {
    return authInfo;
  }

  const profileRepo = getMongoRepository(Profile);

  const header: string = headers.authorization;
  const token: string = (header || '').split(' ').pop();

  const roles = [PERMISSION_EVERYONE];

  if (!token) {
    roles.unshift(PERMISSION_UNAUTHENTICATED);
    return;
  }

  try {
    authInfo = verify(token, Buffer.from(jwtSecret, 'base64')) as IAuthInfo;
  } catch {
    return null;
  }

  const profile = await profileRepo.findOne({
    where: { name: authInfo.user.profileId },
  });
  if (profile) {
    roles.unshift(PERMISSION_AUTHENTICATED, ...profile.roles);
  }

  authInfo = authInfo || { roles };
  authInfo.roles = roles;

  (req as any).authInfo = authInfo;

  return authInfo;
};
