import './modules/graphql/replace-metadata';

import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';

import { MongoModule } from './database/mongo.module';
import { RequestContextMiddleware } from './modules/context/request-context.middleware';
import { CatsModule } from './modules/entities/cats/cats.module';
import { OwnersModule } from './modules/entities/owners/owners.module';
import { ProfilesModule } from './modules/entities/profiles/profiles.module';
import { RolesModule } from './modules/entities/roles/roles.module';
import { UsersModule } from './modules/entities/users/users.module';
import { GraphModule } from './modules/graphql/graphql.module';

@Module({
  imports: [
    RolesModule,
    CatsModule,
    UsersModule,
    ProfilesModule,
    OwnersModule,
    GraphModule,
    MongoModule,
  ],
})
export class AppModule implements NestModule {
  public configure(consumer: MiddlewareConsumer) {
    consumer.apply(RequestContextMiddleware).forRoutes('*');
  }
}
